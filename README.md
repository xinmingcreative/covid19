# covid19

This is as data analysis report for covid-19 happened in Malaysia.

## Data Studio report
see https://datastudio.google.com/reporting/69541506-2ec5-4c92-8a96-cc8cfe7f26ab/

## Tools
1. [PowerBI](https://powerbi.microsoft.com/en-us/desktop/)

[Chart from Google Sphreadsheet](https://docs.google.com/spreadsheets/d/1Un8fWCME6bX44OsNm2M4X1Wk8ZED5rsV0wprQechYWY/edit?usp=sharing)

## Malaysia YTD Covid-19 Cases
![malaysia](https://docs.google.com/spreadsheets/d/1Un8fWCME6bX44OsNm2M4X1Wk8ZED5rsV0wprQechYWY/embed/oimg?id=1Un8fWCME6bX44OsNm2M4X1Wk8ZED5rsV0wprQechYWY&oid=519322519&disposition=ATTACHMENT&bo=false&filetype=png&zx=vfexntc2xs)

## Malaysia Northern New Discovery Cases
![norther](https://docs.google.com/spreadsheets/d/1Un8fWCME6bX44OsNm2M4X1Wk8ZED5rsV0wprQechYWY/embed/oimg?id=1Un8fWCME6bX44OsNm2M4X1Wk8ZED5rsV0wprQechYWY&oid=951566303&disposition=ATTACHMENT&bo=false&filetype=png&zx=fxwvt3gzh469)

## Source
1. https://github.com/MoH-Malaysia/covid19-public
2. https://github.com/CITF-Malaysia/citf-public
1. https://newslab.malaysiakini.com/covid-19/en
2. http://www.moh.gov.my/index.php/pages/view/2019-ncov-wuhan
3. https://powerbi.microsoft.com/en-us/desktop/
3. https://t.me/MKNRasmi
